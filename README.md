My beers in Beer XML format.

# Licence
![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

Feel free to copy or get inspired and give me feedbacks!

# Beer version
My beers recipes are using versioning like a computer software. Each
number correspond to:

* **Major**: Recipe number
* **Minor**: Recipe tweak
* **Bugfix**: Recipe brew number

# Beers codenames
Here is the list of different beer recipes:

## Libre beer (1.x.x)
Beer based on the "[bière libre](https://2012.rmll.info/biere-libre)"
recipe, brewed for the 13th edition of the RMLL (Rencontres Mondiales du
Logiciel Libre)

Recipe lost!

## Blanche d'Armailli (2.x.x)
Smoky Weizen

## Lavandale (3.x.x)
Pale Ale with lavender 

## Telpo (4.x.x)
Pale Ale with Ginger and verbena

Recipe lost!

## Benistout (5.x.x)
Russian Imperial Stout with "vin cuit", that is basically "thick" pear
sugar, a speciality of my area (Fribourg / Switzerland)

# Labels
A big thank to Muggen for drawing the labels!
